from fastapi import APIRouter

router = APIRouter()


@router.get("/", response_model=dict)
async def say_hi_to_the_camera():
    return {"message": "Hello Mom!"}
