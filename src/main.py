from typing import Dict, Union, Any

import httpx
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from supertokens_python import SupertokensConfig, get_all_cors_headers, init, InputAppInfo
from supertokens_python.framework.fastapi import get_middleware
from supertokens_python.recipe import session, thirdparty
from supertokens_python.recipe.thirdparty.provider import Provider
from supertokens_python.recipe.thirdparty.types import AccessTokenAPI, AuthorisationRedirectAPI, UserInfo, UserInfoEmail

from config import VERSION, VATSIM_CLIENT_SECRET, VATSIM_CLIENT_ID
from src import api
from src.vatsimuser import VatsimUser


def get_vatsim_user(access_token: str):
    response = httpx.get("https://auth-dev.vatsim.net/api/user",
                         headers={"Authorization": f'Bearer {access_token}',
                                  "Accept": "application/json"})
    response.raise_for_status()
    return VatsimUser(**response.json()["data"])


class Vatsim(Provider):
    def __init__(self):
        super().__init__('vatsim', False)

    async def get_profile_info(self, auth_code_response: Dict[str, Any], user_context: Dict[str, Any]) -> UserInfo:
        user = get_vatsim_user(access_token=auth_code_response["access_token"])
        return UserInfo(user_id=user.cid,
                        email=UserInfoEmail(email=user.personal.email, email_verified=True))

    def get_authorisation_redirect_api_info(self, user_context: Dict[str, Any]) -> AuthorisationRedirectAPI:
        params: Dict[str, Any] = {
            'scope': 'full_name vatsim_details email country',
            'response_type': 'code',
            'client_id': VATSIM_CLIENT_ID,
        }
        return AuthorisationRedirectAPI(url="https://auth-dev.vatsim.net/oauth/authorize", params=params)

    def get_access_token_api_info(
            self, redirect_uri: str, auth_code_from_request: str, user_context: Dict[str, Any]) -> AccessTokenAPI:
        params = {
            'client_id': VATSIM_CLIENT_ID,
            'client_secret': VATSIM_CLIENT_SECRET,
            'grant_type': 'authorization_code',
            'code': auth_code_from_request,
            'redirect_uri': redirect_uri
        }
        return AccessTokenAPI(url="https://auth-dev.vatsim.net/oauth/token", params=params)

    def get_redirect_uri(self, user_context: Dict[str, Any]) -> Union[None, str]:
        return None

    def get_client_id(self, user_context: Dict[str, Any]) -> str:
        return "<CLIENT ID>"


init(
    app_info=InputAppInfo(
        app_name="Portugal-vACC",
        api_domain="https://profiles.ambitio.cyou",
        website_domain="https://ambitio.cyou",
        api_base_path="/auth",
        website_base_path="/auth"
    ),
    supertokens_config=SupertokensConfig(
        connection_uri="https://try.supertokens.com",
        # api_key="IF YOU HAVE AN API KEY FOR THE CORE, ADD IT HERE"
    ),
    framework='fastapi',
    recipe_list=[
        session.init(),  # initializes session features
        thirdparty.init(
            sign_in_and_up_feature=thirdparty.SignInAndUpFeature(providers=[
                Vatsim()
            ])
        )
    ],
    mode='asgi'  # use wsgi if you are running using gunicorn
)

app = FastAPI(title="A Profiles Pi", docs_url="/api/docs", version=VERSION,
              description=f"A Pi for users and stuff. Boring to be honest...")
app.add_middleware(get_middleware())

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["Content-Type"] + get_all_cors_headers(),
)

app.include_router(api.router, prefix="/api")
