import os

from dotenv import load_dotenv

load_dotenv()

VERSION = os.getenv("VERSION", "Debug :)")

VATSIM_CLIENT_ID = os.getenv("VATSIM_CLIENT_ID")
VATSIM_CLIENT_SECRET = os.getenv("VATSIM_CLIENT_SECRET")
